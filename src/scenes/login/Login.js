import React, {Component} from 'react';
import {StyleSheet,
    View,
    KeyboardAvoidingView,
    Text,
    TextInput,
    Alert,
    Image
} from 'react-native';
import {Button, SocialIcon} from 'react-native-elements';



class FormLogin extends Component {
    render() {
        return (

            <View style={styles.formContainer}>
                <Text style={styles.label}>Email</Text>
                <TextInput style={styles.input}/>
                <Text style={styles.label}>Password</Text>
                <TextInput style={styles.input}/>
            </View>

        );
    }
}

export default class Login extends Component {

    _message(){
        Alert.alert('Login With Faceboob','You are login with Facebook')
    }

    render() {
        return (

            <KeyboardAvoidingView behavior="padding" style={styles.container}>

                {/*Logo*/}
                <View style={styles.logoContainer}>
                    <Image source={require('../../../public/images/logo.png')}/>
                    <Text style={styles.title}>
                        <Text style={{color:'#f53888'}}>Tourism</Text><Text style={{color:'#9cabbc'}}>App</Text>
                    </Text>
                </View>

                {/*Login Form*/}
                <FormLogin/>

                {/*Social Login*/}
                <View style={styles.socialContainer}>

                    <SocialIcon style={{padding:10}}
                        title='Sign In With Facebook'
                        button
                        type='facebook'
                    />
                    <SocialIcon style={{padding:10, backgroundColor:'#e70519'}}
                        title='Sign In With Facebook'
                        button
                        type='google'
                    />

                    {/*<Button*/}
                        {/*onPress={this._message}*/}
                        {/*title="Login with Facebook"*/}
                        {/*color="#fff"*/}
                        {/*accessibilityLabel="Learn more about this purple button"*/}
                    {/*/>*/}
                </View>
                {/*Footer*/}
                <View style={styles.footerContainer}>
                    <Text style={{color:'#9c9c9c'}}>Developed by Yulian-Jimmy-Mina-Portilla</Text>
                </View>
            </KeyboardAvoidingView>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2c3e50'
    },
    logoContainer: {
        alignItems:'center',
        paddingBottom:20
    },
    formContainer: {
        alignSelf: 'stretch',
        backgroundColor: '#f5f5f5',
        padding: 30,
        margin: 20,
        borderRadius:20,

    },
    socialContainer:{
        flexDirection:'row'
    },
    footerContainer:{
        marginTop:10,
    },
    input: {
        height: 40,
        fontSize:20
    },
    label:{
        color:'#9cabbc',
        fontWeight:'bold'
    },
    title:{
        fontSize:50,
        fontWeight:'bold'
    }
});